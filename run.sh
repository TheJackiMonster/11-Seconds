#!/bin/bash
if [ ! -d bin/ ]; then
	./build.sh
fi

java -cp bin/ de.thejackimonster.ggj2018.game.Game
