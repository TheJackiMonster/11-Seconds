package de.thejackimonster.ggj2018.audio;

import java.util.HashMap;
import java.util.Map;

import de.thejackimonster.ggj2018.content.Importer;

public final class Sounds {
	
	private static final Map<String, Audio> sounds;
	
	static {
		sounds = new HashMap<String, Audio>();
	}
	
	public static final Audio getSound(String name) {
		if (sounds.containsKey(name)) {
			return sounds.get(name);
		} else {
			final Audio audio = Importer.loadAudio("audio/" + name);
			
			sounds.put(name, audio);
			
			return audio;
		}
	}

}
