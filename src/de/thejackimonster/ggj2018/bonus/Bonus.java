package de.thejackimonster.ggj2018.bonus;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import de.thejackimonster.ggj2018.audio.Music;
import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.gui.Font;
import de.thejackimonster.ggj2018.gui.Menu;

public final class Bonus {

	public static final EasterEgg[] EGGS = {
		() -> {
			Music.playMusic("music");
		},
		() -> {
			NO_TIMER = true;
		},
		() -> {
			FULL_POWER = true;
		},
		() -> {
			OPEN_END = true;
		},
		() -> {
			SECRET_ENDING = true;
		}
	};
	
	public static boolean EASTER_EGGS = false;
	public static boolean NO_TIMER = false;
	public static boolean FULL_POWER = false;
	public static boolean OPEN_END = false;
	public static boolean SECRET_ENDING = false;

	private static BufferedImage secret = null;

	public static final float MENU_TIME = 30.0f;

	public static final void drawSecret(Graphics gfx, float time, int width, int height) {
		if (secret == null) {
			secret = Importer.loadImage("bonus/secret");
		}
		
		final int offset_y = Math.round(height - 1.5f * time * height / MENU_TIME);
		
		gfx.drawImage(
			secret, 
			(width - secret.getWidth()) / 2, 
			(height - secret.getHeight()) / 2 + offset_y, 
			(width + secret.getWidth()) / 2,
			(height + secret.getHeight()) / 2 + offset_y, 
			0, 0, secret.getWidth(), secret.getHeight(),
			null
		);
		
		final String msg = "Thanks for playing!";
		final int size = 28;
		
		Font.drawString(gfx, msg, (width - msg.length() * size) / 2, size / 2 + offset_y, size, Color.WHITE);
		
		if (time / MENU_TIME > 0.75f) {
			Menu.drawAuthor(gfx, width, height, size * 3 / 4, Color.WHITE);
		}
	}
	
}
