package de.thejackimonster.ggj2018.bonus;

public interface EasterEgg {
	
	public void trigger();
	
}
