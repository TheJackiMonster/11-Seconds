package de.thejackimonster.ggj2018.entity;

import de.thejackimonster.ggj2018.item.Item;
import de.thejackimonster.ggj2018.world.Level;

public final class EntityHat extends EntityItem {

	public EntityHat(Level lvl, int ex, int ey) {
		super("hat", Item.HAT_ID, lvl, ex, ey);
	}
	
	@Override
	public final void trigger(float[] itemStates) {
		itemStates[Item.CLOCK_ID] += 10.0f;
		itemStates[ID] += 10.0f;
	}

}
