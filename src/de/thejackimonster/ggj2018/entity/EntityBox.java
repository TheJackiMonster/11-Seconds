package de.thejackimonster.ggj2018.entity;

import java.awt.Graphics;

import de.thejackimonster.ggj2018.world.Level;

public class EntityBox extends Entity {
	
	private final int boxID;

	protected EntityBox(String name, Level lvl, int ex, int ey) {
		super(name, lvl, ex, ey);
		boxID = (hashCode() & 3);
	}
	
	public EntityBox(Level lvl, int ex, int ey) {
		this("box2", lvl, ex, ey);
	}
	
	@Override
	public void draw(Graphics gfx, int x, int y, int width, int height) {
		gfx.drawImage(
			texture,
			x, y, x+width, y+height,
			boxID * texture.getWidth() / 4,
			0,
			(boxID + 1) * texture.getWidth() / 4, 
			texture.getHeight(),
			null
		);
	}

}
