package de.thejackimonster.ggj2018.entity;

import de.thejackimonster.ggj2018.item.Item;
import de.thejackimonster.ggj2018.world.Level;

public abstract class EntityItem extends Entity {
	
	public final int ID;

	public EntityItem(String name, int itemID, Level lvl, int ex, int ey) {
		super(name, lvl, ex, ey);
		
		if ((itemID < 0) || (itemID >= Item.TYPES.length)) {
			ID = -1;
		} else {
			ID = itemID;
		}
	}

	public abstract void trigger(float[] itemStates);

}
