package de.thejackimonster.ggj2018.data;

import de.thejackimonster.ggj2018.bonus.Bonus;
import de.thejackimonster.ggj2018.world.Level;

public final class DataFinish extends Data {
	
	public boolean open;
	
	public DataFinish(Level lvl, int dx, int dy) {
		super(lvl, dx, dy);
		open = false;
	}

	@Override
	public void update(float deltaTime) {
		boolean anyButtonLeft = false;
		
		if (!Bonus.OPEN_END) {
			for (final Data data : level.allData()) {
				if ((data instanceof DataButton) &&
					(!((DataButton) data).pressed)) {
					anyButtonLeft = true;
					break;
				}
			}
		}
		
		open = !anyButtonLeft;
	}
	
}
