package de.thejackimonster.ggj2018.data;

import java.util.Random;

import de.thejackimonster.ggj2018.world.Level;

public class Data {

	private static final Random random = new Random();
	
	protected final Level level;
	
	protected final int x;
	protected final int y;
	
	public Data(Level lvl, int dx, int dy) {
		level = lvl;
		x = dx;
		y = dy;
	}
	
	public void update(float deltaTime) {}
	
	public void stateUpdate() {}

	public static final int getDetail(Data data) {
		if (data != null) {
			random.setSeed(data.x + data.y * data.level.width);
		} else {
			random.setSeed(0L);
		}
		
		return random.nextInt();
	}

}
