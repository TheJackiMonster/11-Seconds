package de.thejackimonster.ggj2018.game;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import de.thejackimonster.ggj2018.audio.Music;
import de.thejackimonster.ggj2018.audio.Sounds;
import de.thejackimonster.ggj2018.bonus.Bonus;
import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.entity.Entity;
import de.thejackimonster.ggj2018.entity.EntityPlayer;
import de.thejackimonster.ggj2018.gui.ColorTable;
import de.thejackimonster.ggj2018.gui.Digits;
import de.thejackimonster.ggj2018.gui.Font;
import de.thejackimonster.ggj2018.gui.Menu;
import de.thejackimonster.ggj2018.world.Level;

public class Game implements KeyListener {
	
	public static final String TITLE = "11 Seconds...";
	public static final String AUTHOR = "TheJackiMonster";
	
	private static final int WIDTH = 800;
	private static final int HEIGHT = 600;
	
	private static final int TILE_SIZE = 70;
	
	private static final float FPS_SYNC = 60.0f;
	
	private final JFrame frame;
	private final Canvas canvas;
	
	private GameState state;
	private float waitTime;
	
	private Level level;
	
	private int player_idx = -1;
	private int level_idx = 0x00;
	
	private float camera_x;
	private float camera_y;
	
	private Game() {
		frame = new JFrame(TITLE);
		
		frame.setTitle(TITLE);
		frame.setSize(WIDTH, HEIGHT);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		final List<Image> icons = new ArrayList<Image>();
		
		final Image icon16x16 = Importer.loadImage("icon16x16", true);
		final Image icon32x32 = Importer.loadImage("icon32x32", true);
		final Image icon64x64 = Importer.loadImage("icon64x64", true);
		final Image icon128x128 = Importer.loadImage("icon128x128", true);
		
		if (icon128x128 != null) icons.add(icon128x128);
		if (icon64x64 != null) icons.add(icon64x64);
		if (icon32x32 != null) icons.add(icon32x32);
		if (icon16x16 != null) icons.add(icon16x16);
		
		frame.setIconImages(icons);
		
		canvas = new Canvas();
		
		canvas.setBackground(Color.BLACK);
		canvas.setSize(WIDTH, HEIGHT);
		canvas.addKeyListener(this);
		
		frame.add(canvas);
		
		frame.setVisible(true);
		
		state = GameState.INIT_MENU;
	}
	
	public void loadLevel(int id) {
		try {
			level = new Level(id);
			
			level_idx = id;
			
			if (level.players.length > 0) {
				player_idx = 0;
				
				camera_x = level.players[player_idx].getX();
				camera_y = level.players[player_idx].getY();
				
				level.players[player_idx].setTime(10.0f);
			}
			
			level.stateUpdate();
			
			waitTime = 3.45f;
			state = GameState.WAIT_MENU;
		} catch (IOException e) {
			if (Bonus.SECRET_ENDING) {
				Sounds.getSound("bird").play();
				
				Music.playMusic("music");
				
				waitTime = Bonus.MENU_TIME;
				state = GameState.BONUS_MENU;
			} else {
				state = GameState.INIT_MENU;
			}
		}
	}
	
	public void update(float deltaTime) {
		if (state.doUpdates) {
			if ((player_idx >= 0) && (level.players.length > 0)) {
				final float f = deltaTime * 2.0f;
				
				camera_x = camera_x * Math.max(1.0f - f, 0.0f)
						 + level.players[player_idx].getX() * Math.min(f, 1.0f);
				camera_y = camera_y * Math.max(1.0f - f, 0.0f)
						 + level.players[player_idx].getY() * Math.min(f, 1.0f);
			}
			
			level.update(deltaTime);
			
			if (getTime() <= 0.0f) {
				Sounds.getSound("game_over2").play();
				
				loadLevel(level_idx);
			}
		} else {
			switch (state) {
			case INIT_MENU:
				if (canvas.hasFocus()) {
					state = GameState.START_MENU;
				}
				
				break;
			case START_MENU:
				break;
			case HELP_MENU:
				break;
			case WAIT_MENU:
				final int last_sec = Math.round(waitTime + 0.5f);
				
				waitTime = Math.max(waitTime - deltaTime, 0.0f);
				
				final int now_sec = Math.round(waitTime + 0.5f);
				
				if (waitTime <= 0.0f) {
					if (now_sec < last_sec) {
						Sounds.getSound("level").play();
					}
					
					state = GameState.INGAME;
				} else {
					if (now_sec < last_sec) {
						Sounds.getSound("wait2").play();
					}
				}
				
				break;
			case BONUS_MENU:
				waitTime = Math.max(waitTime - deltaTime, 0.0f);
				
				if (waitTime <= 0.0f) {
					state = GameState.INIT_MENU;
				}
				
				break;
			default:
				state = GameState.INIT_MENU;
				break;
			}
		}
	}
	
	public final void stateUpdate() {
		if (state.doStateUpdates) {
			level.stateUpdate();
			
			for (final EntityPlayer player : level.players) {
				if (player.hasWon()) {
					Sounds.getSound("finish2").play();
					
					try {
						Thread.sleep(250L);
					} catch (InterruptedException e) {}
					
					loadLevel(++level_idx);
					break;
				}
			}
		}
	}
	
	public void render() {
		final BufferStrategy bs = canvas.getBufferStrategy();
		
		if (bs == null) {
			canvas.createBufferStrategy(3);
			return;
		}
		
		final Graphics gfx = bs.getDrawGraphics();
		
		gfx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		
		if (state.doRendering) {
			renderIngame(gfx);
		}
		
		switch (state) {
		case INIT_MENU: {
			Menu.drawBackground(gfx, canvas.getWidth(), canvas.getHeight());
			
			final double time = System.currentTimeMillis() * 0.0125;
			final int size = (int) (canvas.getHeight() * 6 / (70 + Math.cos(time) * 5));
			
			Menu.drawTitle(gfx, canvas.getWidth(), canvas.getHeight(), size, new Color(
				121, 170, 171
			));
			
			Menu.drawAuthor(gfx, canvas.getWidth(), canvas.getHeight(), 24, Color.GRAY);
			break;
		} case START_MENU: {
			Menu.drawBackground(gfx, canvas.getWidth(), canvas.getHeight());
			
			final double time = System.currentTimeMillis() * 0.0125;
			final int size = (int) (canvas.getHeight() * 6 / (70 + Math.cos(time) * 5));
			
			Menu.drawTitle(gfx, canvas.getWidth(), canvas.getHeight(), size, new Color(
				205, 181, 107
			), "Press any key to continue");
			
			Menu.drawAuthor(gfx, canvas.getWidth(), canvas.getHeight(), 24, Color.GRAY);
			break;
		} case HELP_MENU: {
			Menu.drawBackground(gfx, canvas.getWidth(), canvas.getHeight());
			
			final Color color = new Color(152, 112, 172);
			
			Font.drawString(
				gfx,
				" - Movement:\n" +
				"  - Up:             [W/UP]\n"+
				"  - Left:           [A/LEFT]\n"+
				"  - Down:           [S/DOWN]\n"+
				"  - Right:          [D/RIGHT]\n"+
				"\n"+
				" - Change Player:   [SPACE]\n"+
				" - Restart Level:   [R]\n"+
				" - Previous Level:  [BACKSPACE]\n"+
				" - Exit Game:       [ESCAPE]\n"
				, canvas.getWidth() / 40,
				canvas.getHeight() / 3, 
				16,
				color
			);
			
			final String msg = "Press any key to continue...";
			
			Font.drawString(gfx, msg, (canvas.getWidth() - msg.length() * 16) / 2, canvas.getHeight() - 34, 16, color);
			
			break;
		} case WAIT_MENU: {
			gfx.setColor(new Color(0, 0, 0, 125));
			gfx.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
			
			final int sec = Math.round(waitTime);
			
			Digits.drawDigits(
				gfx, sec, 1,
				canvas.getWidth() / 2 - 19, 
				canvas.getHeight() / 2 - 19, 
				38, ColorTable.pickColor(sec, 3, -1)
			);
			
			break;
		} case BONUS_MENU: {
			Bonus.drawSecret(gfx, Bonus.MENU_TIME - waitTime, canvas.getWidth(), canvas.getHeight());
			break;
		} default:
			break;
		}
		
		bs.show();
	}
	
	private final void renderIngame(Graphics gfx) {
		final int view_x = (canvas.getWidth() + TILE_SIZE-1) / TILE_SIZE;
		final int view_y = (canvas.getHeight() + TILE_SIZE-1) / TILE_SIZE;
		
		final int scroll_x = Math.round((camera_x - view_x * 0.5f + 0.75f) * TILE_SIZE);
		final int scroll_y = Math.round((camera_y - view_y * 0.5f + 0.75f) * TILE_SIZE);
		
		final int offset_x = scroll_x / TILE_SIZE;
		final int offset_y = scroll_y / TILE_SIZE;
		
		for (int y = -1; y < view_y + 1; y++) {
			for (int x = -1; x < view_x + 1; x++) {
				level.getTile(
					x + offset_x,
					y + offset_y
				).draw(
					gfx, 
					x * TILE_SIZE - scroll_x + offset_x * TILE_SIZE, 
					y * TILE_SIZE - scroll_y + offset_y * TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE, 
					level.getData(
						x + offset_x, y + offset_y
					)
				);
			}
		}
		
		for (int y = -2; y < view_y + 2; y++) {
			for (int x = -2; x < view_x + 2; x++) {
				final Entity entity = level.getEntity(
					x + offset_x,
					y + offset_y
				);
				
				if (entity != null) {
					entity.draw(
						gfx,
						x * TILE_SIZE - scroll_x + offset_x * TILE_SIZE, 
						y * TILE_SIZE - scroll_y + offset_y * TILE_SIZE, 
						TILE_SIZE,
						TILE_SIZE
					);
				}
			}
		}
		
		if (!Bonus.NO_TIMER) {
			final int sec = Math.round(getTime());
			
			Digits.drawDigits(gfx, sec, 2, 12, 12, 28, ColorTable.pickColor(sec, -1, 11));
		}
	}
	
	public final boolean isRunning() {
		return frame.isVisible();
	}
	
	public final float getTime() {
		float time = 0.0f;
		
		for (final EntityPlayer player : level.players) {
			time += player.getTime();
		}
		
		return time;
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	
	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		final int code = e.getKeyCode();
		
		if (state == GameState.START_MENU) {
			if (code == KeyEvent.VK_ESCAPE) {
				System.exit(0);
			} else {
				state = GameState.HELP_MENU;
			}
			
			return;
		} else
		if (state == GameState.HELP_MENU) {
			int id = 0x000;
			
			if (e.isShiftDown()) {
				switch (code) {
				case KeyEvent.VK_0:
					id = 0x000;
					break;
				case KeyEvent.VK_1:
					id = 0x001;
					break;
				case KeyEvent.VK_2:
					id = 0x002;
					break;
				case KeyEvent.VK_3:
					id = 0x003;
					break;
				case KeyEvent.VK_4:
					id = 0x004;
					break;
				case KeyEvent.VK_5:
					id = 0x005;
					break;
				case KeyEvent.VK_6:
					id = 0x006;
					break;
				case KeyEvent.VK_7:
					id = 0x007;
					break;
				case KeyEvent.VK_8:
					id = 0x008;
					break;
				case KeyEvent.VK_9:
					id = 0x009;
					break;
				case KeyEvent.VK_A:
					id = 0x00A;
					break;
				case KeyEvent.VK_B:
					id = 0x00B;
					break;
				case KeyEvent.VK_C:
					id = 0x00C;
					break;
				case KeyEvent.VK_D:
					id = 0x00D;
					break;
				case KeyEvent.VK_E:
					id = 0x00E;
					break;
				case KeyEvent.VK_F:
					id = 0x00F;
					break;
				default:
					break;
				}
			}
			
			loadLevel(id);
			return;
		}
		
		if (state == GameState.INGAME) {
			if ((code == KeyEvent.VK_UP) || (code == KeyEvent.VK_W)) {
				if ((player_idx >= 0) && (level.players.length > 0)) {
					level.players[player_idx].move(0, -1);
				}
			} else
			if ((code == KeyEvent.VK_DOWN) || (code == KeyEvent.VK_S)) {
				if ((player_idx >= 0) && (level.players.length > 0)) {
					level.players[player_idx].move(0, +1);
				}
			} else
			if ((code == KeyEvent.VK_LEFT) || (code == KeyEvent.VK_A)) {
				if ((player_idx >= 0) && (level.players.length > 0)) {
					level.players[player_idx].move(-1, 0);
				}
			} else
			if ((code == KeyEvent.VK_RIGHT) || (code == KeyEvent.VK_D)) {
				if ((player_idx >= 0) && (level.players.length > 0)) {
					level.players[player_idx].move(+1, 0);
				}
			} else
			if (code == KeyEvent.VK_SPACE) {
				if (level.players.length > 0) {
					player_idx = (player_idx + 1) % level.players.length;
				}
			} else
			if (code == KeyEvent.VK_R) {
				loadLevel(level_idx);
				return;
			} else
			if ((code == KeyEvent.VK_BACK_SPACE) && (level_idx > 0)) {
				loadLevel(--level_idx);
				return;
			}
			
			stateUpdate();
		}
		
		if (code == KeyEvent.VK_ESCAPE) {
			state = GameState.INIT_MENU;
		} else
		if (code == KeyEvent.VK_F11) {
			frame.setExtendedState(frame.getExtendedState() ^ JFrame.MAXIMIZED_BOTH);
		}
	}

	public static void main(String[] args) {
		final Game game = new Game();
		
		long lastTime = System.currentTimeMillis();
		long currentTime, deltaTime;
		
		float sync = 0.0f;
		int frames = 0;
		
		while (game.isRunning()) {
			currentTime = System.currentTimeMillis();
			deltaTime = Math.abs(currentTime - lastTime);
			lastTime = currentTime;
			
			game.update(0.001f * deltaTime);
			
			sync += 0.001f * FPS_SYNC * deltaTime;
			
			if (sync > frames) {
				game.render();
				frames++;
				
				if (frames >= FPS_SYNC) {
					sync -= FPS_SYNC;
					frames = 0;
				}
			}
			
			try {
				Thread.sleep(10L);
			} catch (InterruptedException e) {
				break;
			}
		}
	}
	
}
