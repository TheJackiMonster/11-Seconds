package de.thejackimonster.ggj2018.tile;

import java.awt.Graphics;

import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.data.DataFinish;
import de.thejackimonster.ggj2018.world.Level;

public final class TileFinish extends Tile {

	public TileFinish() {
		super("finish", false);
	}
	
	@Override
	public final Data createData(Level level, int x, int y) {
		return new DataFinish(level, x, y);
	}
	
	@Override
	public final void draw(Graphics gfx, int x, int y, int width, int height, Data data) {
		final int cut = texture.getWidth() / 2;
		final int srcX = (((DataFinish) data).open? cut : 0);
		
		gfx.drawImage(
			texture,
			x, y, x+width, y+height,
			srcX, 0, srcX + cut, texture.getHeight(),
			null
		);
	}

}
